# syntax=docker/dockerfile:1

# dev container
FROM golang:1.18-bullseye as dev
WORKDIR /app

# build container
FROM golang:1.18-bullseye as build
WORKDIR /app
COPY go.mod ./
COPY *.go ./
RUN go build -o /app/hello-go

# production container
FROM gcr.io/distroless/base-debian11 as runtime
# LABEL maintainer="The Prometheus Authors <prometheus-developers@googlegroups.com>"
WORKDIR /
COPY --from=build /app/hello-go /hello-go
USER nonroot:nonroot
CMD ["/hello-go"]
