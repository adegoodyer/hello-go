# hello-go

## Overview
- base template for go projects
- simple hello world example with test

## Development Container
- mount entire directory into debian development container
- Go already installed

## Build Container
- copy Go specific files into container
- runs Go tests

## Production Container
- uses [distroless base image](https://github.com/GoogleContainerTools/distroless/blob/main/README.md)
- contains nothing else but application binary and it's dependencies
- cannot shell into container
- very small image size
- very secure

## Task Commands
```bash
task list # list of tasks
task --summary <task> # summary of task

task dev:shell  # shell into development container
task dev:test   # build development container, run tests and execute application
task prod:run   # build and run prod container
```
